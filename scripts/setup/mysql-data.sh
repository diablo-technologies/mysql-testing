#!/bin/bash
# 
#  another stupid simple modular script
#
#  performs mysql data dir clean reset with symlink update and purge of any existing data
#  all data is nuked from orbit
#
#  $1 = new mysql data dir mount point

rm -rf /var/lib/mysql

ln -s $1 /var/lib/mysql

rm -rf /var/lib/mysql/*
