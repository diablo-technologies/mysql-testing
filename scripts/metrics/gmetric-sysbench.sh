#/bin/bash

OUTPUT=$1

STATS=`echo $OUTPUT | grep -o "] thread.*"`

if [[ ! -z $STATS ]]
then

echo $STATS

THREADS=`echo $STATS | cut -d, -f1 | sed 's/.*threads://'`
TPS=`echo $STATS | cut -d, -f2 | sed 's/.*tps://'`
READS=`echo $STATS | cut -d, -f3| sed 's/.*reads://'`
WRITES=`echo $STATS | cut -d, -f4 | sed 's/.*writes://'`
LATENCY=`echo $STATS | cut -d, -f5 | sed 's/.*time://' | cut -d'm' -f1` 
ERRORS=`echo $STATS | cut -d, -f6 | sed 's/.*errors://'`
RECONNECTS=`echo $STATS | cut -d, -f7 | sed 's/.*reconnects://'`


/usr/bin/gmetric --type "int32" --name "sysbench_threads" --value $THREADS --unit "sb threads"
/usr/bin/gmetric --type "float" --name "sysbench_tps" --value $TPS --unit "trx/sec"
/usr/bin/gmetric --type "float" --name "sysbench_reads" --value $READS --unit "reads"
/usr/bin/gmetric --type "float" --name "sysbench_writes" --value $WRITES --unit "writes"
/usr/bin/gmetric --type "float" --name "sysbench_latency" --value $LATENCY --unit "ms"
/usr/bin/gmetric --type "float" --name "sysbench_errors" --value $ERRORS --unit "errors"
/usr/bin/gmetric --type "float" --name "sysbench_reconnects" --value $RECONNECTS --unit "reconnects"

fi
