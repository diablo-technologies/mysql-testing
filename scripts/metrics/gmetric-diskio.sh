#####
# Just gets iostat metrics and injects them into ganglia
#
# $1 = device id (sdb,md0,etc), $2 = drive make/type (Intel NVMe)

M=`iostat -p $1 -x -k 2 2 | grep $1 | tail -1`

RPS=`echo $M | awk '{print $4}'`
WPS=`echo $M | awk '{print $5}'`
RKBPS=`echo $M | awk '{print $6}'`
WKBPS=`echo $M | awk '{print $7}'`
RAWAIT=`echo $M | awk '{print $11}'`
WAWAIT=`echo $M | awk '{print $12}'`

/usr/bin/gmetric --type "float" --name "$2_Disk_Reads_per_sec" --value $RPS --unit "reads"
/usr/bin/gmetric --type "float" --name "$2_Disk_Writes_per_sec" --value $WPS --unit "writes"
/usr/bin/gmetric --type "float" --name "$2_Disk_Read_KB_per_sec" --value $RKBPS --unit "KB/s"
/usr/bin/gmetric --type "float" --name "$2_Disk_Write_KB_per_sec" --value $WKBPS --unit "KB/s"
/usr/bin/gmetric --type "float" --name "$2_Disk_Read_await" --value $RAWAIT --unit "ms"
/usr/bin/gmetric --type "float" --name "$2_Disk_Write_await" --value $WAWAIT --unit "ms"
