#!/bin/bash
#
# helper script for ansible playbook initiated test runs
# allows secondary process to kick off in 'emo' mode (detached & disowned)
# oltp vars are just passed through; uses same as target script
#
# $1 = %write [60, 80, 100]
# $2 = threads
# $3 = max time
# $4 = # rows per table
# $5 = # of tables
# $6 = restart mysql & rewarm DB? [true, false]
#
# $7 = testid (auto-assigned from test lab app)
#

# check if minimum number of params have been provided
if [ "$#" -lt 7 ]
then
  echo "ERROR: Input variables missing or invalid"
  exit 1
fi

CMD="/git/mysql-testing/scripts/sysbench/start-sysbench.sh $1 $2 $3 $4 $5 $6"

nohup $CMD > /git/mysql-testing/scripts/sysbench/logs/$7.log &
