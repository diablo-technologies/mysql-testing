#!/bin/bash
# 
# simple script to warm-up database (buffer into cache)
# specific to sysbench generated oltp test tables only
# 
# inputs: $1 = database, $2 = number of tables
#

echo `date`
echo Starting table warmup

for i in $(seq 1 $2); do echo $1.sbtest$i; mysql --login-path=local -e "SELECT c FROM $1.sbtest$i WHERE c IS NOT NULL" &> /dev/null; done

echo `date`
echo Warmup complete
