#!/bin/bash
# 
# ganglia repeats last reported value; cheap reset script
#

for ((n=0;n<2;n++))
do

/usr/bin/gmetric --type "int32" --name "sysbench_threads" --value 0 --unit "sb threads"
/usr/bin/gmetric --type "float" --name "sysbench_tps" --value 0 --unit "trx/sec"
/usr/bin/gmetric --type "float" --name "sysbench_reads" --value 0 --unit "reads"
/usr/bin/gmetric --type "float" --name "sysbench_writes" --value 0 --unit "writes"
/usr/bin/gmetric --type "float" --name "sysbench_latency" --value 0 --unit "ms"
/usr/bin/gmetric --type "float" --name "sysbench_errors" --value 0 --unit "errors"
/usr/bin/gmetric --type "float" --name "sysbench_reconnects" --value 0 --unit "reconnects"
sleep 10

done
