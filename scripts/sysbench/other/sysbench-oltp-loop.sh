#!/bin/bash

# runs loops for specified threads against 32Tx20MM database for 5/10/15 minute limits

for thread in 1 4 8 28 56; do ./sysbench-oltp.sh $thread 32 20000000 test 300; done
for thread in 1 4 8 28 56; do ./sysbench-oltp.sh $thread 32 20000000 test 600; done
for thread in 1 4 8 28 56; do ./sysbench-oltp.sh $thread 32 20000000 test 900; done
