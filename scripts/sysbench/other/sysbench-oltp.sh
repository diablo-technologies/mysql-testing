#!/bin/bash
#
# simple script to run oltp tests
#  
# input params: $1 = thread count, $2 = table count, $3 = table size, $4 = db, $5 = max time

UTIME=`date +%s`
TESTMETA=threads-$1-tables-$2-rows-$3-db-$4-maxtime-$5-$UTIME

mkdir -p oltp/my.conf

cp /etc/my.cnf ./oltp/my.conf/my-$TESTMETA.cnf

RESULT_FILE=./oltp/sysbench-$TESTMETA.txt

echo `date` + " : Running threads test (threads=$1, tables=$2, rows=$3, db=$4, max-time=$5) -> $RESULT_FILE"


sysbench --test=/usr/share/doc/sysbench/tests/db/oltp.lua --oltp-table-size=$3 --oltp-tables-count=$2 --mysql-db=$4 --mysql-user=root --mysql-password=Startup2014 --max-time=$5 --max-requests=0 --num-threads=$1 run &> $RESULT_FILE

sleep 10
