#!/bin/bash
#
# simple script to just run the cpu prime number test and persist results to disk
#  
# input params: $1 = thread count

mkdir -p cpu

RESULT_FILE=./cpu/sysbench-cpu-prime-20000-threads-$1.txt

echo "Running CPU prime test (max=20000, threads=$1) -> $RESULT_FILE"

sysbench --test=cpu --cpu-max-prime=20000 --num-threads=$1 run &> $RESULT_FILE

