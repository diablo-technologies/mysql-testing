#!/bin/bash
#
# simple script to collect thread subsystem performance 
#  
# input params: $1 = thread count

mkdir -p threads

RESULT_FILE=./threads/sysbench-threads-$1.txt

echo "Running threads test (threads=$1) -> $RESULT_FILE"

sysbench --test=threads --num-threads=$1 run &> $RESULT_FILE

