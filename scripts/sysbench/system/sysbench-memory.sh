#!/bin/bash
#
# simple script to run memory test and persist results to disk
#  
# input params: $1 = thread count, $2 = memory test operation mode, $3 = memory size, $4 = memory block size

mkdir -p memory

RESULT_FILE=./memory/sysbench-memory-mode-$2-memsize-$3-blocksize-$4-threads-$1.txt

echo "Running memory test (threads=$1, memory-mode=$2, memory size=$3, mem block size=$4) -> $RESULT_FILE"

sysbench --test=memory --memory-oper=$2 --memory-total-size=$3 --memory-block-size=$4 --num-threads=$1 run &> $RESULT_FILE

