#!/bin/bash
#
# simple script to run file io tests
#  
# input params: $1 = mode, $2 = thread count

mkdir -p fileio

RESULT_FILE=./fileio/sysbench-mode-$1-threads-$2.txt

echo "Running file I/O test (threads=$2, mode=$1) -> $RESULT_FILE"

sysbench --test=fileio --file-total-size=300G --file-test-mode=$1 --max-time=300 --max-requests=0 --file-block-size=64K --file-num=30 --num-threads=$2 run &> $RESULT_FILE
sleep 10;
