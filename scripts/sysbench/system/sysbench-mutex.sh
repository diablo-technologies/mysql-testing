#!/bin/bash
#
# simple script to run mutex tests
#  
# input params: $1 = thread count

mkdir -p mutex

RESULT_FILE=./mutex/sysbench-cpu-mutex-threads-$1.txt

echo "Running mutex test (threads=$1) -> $RESULT_FILE"

sysbench --test=mutex --num-threads=$1 run &> $RESULT_FILE

