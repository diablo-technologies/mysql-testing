#!/bin/bash
#
# master start script to kick off sysbench and enable ganglia sysbench metric collection
#
# $1 = %write [60, 80, 100]
# $2 = threads
# $3 = max time
# $4 = # rows per table
# $5 = # of tables
# $6 = restart mysql & rewarm DB? [true, false/null]
# 


# check if a sysbench related process is still running
if pgrep -f "sysbench --test"> /dev/null
then
    echo "ERROR: Looks like a sysbench test is already running"
    exit 1
fi


# check if minimum number of params have been provided
if [ "$#" -lt 5 ]
then
  echo " "
  echo "Usage: start-sysbench.sh [write %=60,80,100] [max time (s)] [# rows] [# tables] [restart+rewarm db=true, false/null]"
  echo " " 
  echo "e.g. start-sysbench.sh 60 64 7200 1000000 350"
  echo " "
  exit 1
fi

TIMESTAMP=`date +%s`
echo "-------------------------"
echo `date` 
echo "UNIX time:" $TIMESTAMP
echo "Starting sysbench job"
echo "writes:" $1"%"
echo "threads:" $2
echo "max time:" $3
echo "rows per table:" $4
echo "total tables:" $5
echo "restart db:" $6
echo "-------------------------"

# snapshot my.conf
mkdir -p /git/mysql-testing/scripts/sysbench/logs
cp /etc/my.cnf /git/mysql-testing/scripts/sysbench/logs/$TIMESTAMP.my.cnf
echo "my.cnf snapshot saved to /git/mysql-testing/scripts/sysbench/logs/$TIMESTAMP.my.cnf"


echo " "
echo "Resetting sysbench Ganglia metrics to 0 for new run"
/git/mysql-testing/scripts/sysbench/tools/sysbench-reset-metrics-to-0.sh &> /dev/null

# global variables
USER='root'
PASS='Startup2014'
DB='benchmark'
THREADS=$2
MAX_TIME=$3
STAT_INTERVAL=20
TABLE_SIZE=$4
TABLES_COUNT=$5


# restart and rewarm db if enabled
if [ "$6" == "true" ]; then
# restart mysql service
echo "Restarting mysql on existing mount point..."
systemctl restart  mysqld.service
echo " "

# warmup database cache
echo "Warming up $TABLES_COUNT tables in $DB..."
/git/mysql-testing/scripts/sysbench/tools/sysbench-oltp-warmup.sh $DB $TABLES_COUNT 
echo " "
fi


echo "Starting $1% writes test..." 
/git/mysql-testing/scripts/sysbench/tests/sysbench-oltp-test.sh $USER $PASS $DB $THREADS $MAX_TIME $STAT_INTERVAL $TABLE_SIZE $TABLES_COUNT $1 | while read -r line; do /git/mysql-testing/scripts/metrics/gmetric-sysbench.sh "$line"; done
