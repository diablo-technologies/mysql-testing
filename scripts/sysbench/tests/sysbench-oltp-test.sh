#!/bin/bash

# global parameters
USER=$1
PASS=$2
DB=$3
THREADS=$4
MAX_TIME=$5
STAT_INTERVAL=$6
TABLE_SIZE=$7
TABLES_COUNT=$8
WRITE_PCT=$9
TEST='/usr/share/doc/sysbench/tests/db/oltp.lua'

# default local test variables, all enabled for one run each
RANGE_SIZE=100
POINT_SELECTS=1
SIMPLE_RANGES=1
SUM_RANGES=1
ORDER_RANGES=1
DISTINCT_RANGES=1
INDEX_UPDATES=1
NON_INDEX_UPDATES=1

# apply specific write pct ratios
if [ $WRITE_PCT -eq 60 ]; then
RANGE_SIZE=100
POINT_SELECTS=1
SIMPLE_RANGES=1
SUM_RANGES=0
ORDER_RANGES=0
DISTINCT_RANGES=0
INDEX_UPDATES=1
NON_INDEX_UPDATES=0
fi

if [ $WRITE_PCT -eq 80 ]; then
RANGE_SIZE=100
POINT_SELECTS=1
SIMPLE_RANGES=1
SUM_RANGES=0
ORDER_RANGES=0
DISTINCT_RANGES=0
INDEX_UPDATES=2
NON_INDEX_UPDATES=0
fi

if [ $WRITE_PCT -eq 100 ]; then
RANGE_SIZE=100
POINT_SELECTS=0
SIMPLE_RANGES=0
SUM_RANGES=0
ORDER_RANGES=0
DISTINCT_RANGES=0
INDEX_UPDATES=1
NON_INDEX_UPDATES=0
fi


sysbench --test=$TEST --oltp-table-size=$TABLE_SIZE --oltp-tables-count=$TABLES_COUNT --mysql-db=$DB --mysql-user=$USER --mysql-password=$PASS --num-threads=$THREADS --max-requests=0  --max-time=$MAX_TIME --report-interval=$STAT_INTERVAL --oltp-read-only=off --oltp-point-selects=$POINT_SELECTS  --oltp-simple-ranges=$SIMPLE_RANGES --oltp-sum-ranges=$SUM_RANGES --oltp-order-ranges=$ORDER_RANGES --oltp-distinct-ranges=$DISTINCT_RANGES --oltp-index-updates=$INDEX_UPDATES --oltp_non_index_updates=$NON_INDEX_UPDATES --rand-init=on --rand-type=uniform run 

echo "-------------------------"
echo `date`
echo "Test run completed
echo "-------------------------"

/git/mysql-testing/scripts/sysbench/tools/sysbench-reset-metrics-to-0.sh &> /dev/null
