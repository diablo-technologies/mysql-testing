#!/bin/bash

# global parameters

USER=$1
PASS=$2
DB=$3
THREADS=$4
MAX_TIME=$5
STAT_INTERVAL=$6
TABLE_SIZE=$7
TABLES_COUNT=$8

# local test variables

TEST='/usr/share/doc/sysbench/tests/db/oltp.lua'
RANGE_SIZE=100
POINT_SELECTS=1
SIMPLE_RANGES=1
SUM_RANGES=0
ORDER_RANGES=0
DISTINCT_RANGES=0
INDEX_UPDATES=1
NON_INDEX_UPDATES=0


sysbench --test=$TEST --oltp-table-size=$TABLE_SIZE --oltp-tables-count=$TABLES_COUNT --mysql-db=$DB --mysql-user=$USER --mysql-password=$PASS --num-threads=$THREADS --max-requests=0  --max-time=$MAX_TIME --report-interval=$STAT_INTERVAL --oltp-read-only=off --oltp-point-selects=$POINT_SELECTS  --oltp-simple-ranges=$SIMPLE_RANGES --oltp-sum-ranges=$SUM_RANGES --oltp-order-ranges=$ORDER_RANGES --oltp-distinct-ranges=$DISTINCT_RANGES --oltp-index-updates=$INDEX_UPDATES --oltp_non_index_updates=$NON_INDEX_UPDATES --rand-init=on --rand-type=uniform run 
